from flask import Flask,render_template,request,redirect,flash
from .database import db
from .app import app
from .models import User,Product





@app.route('/home')
def home():
    product_list = [
        {"product_name":"tomato1","quantity":20,"price":10.90},
        {"product_name":"tomato2","quantity":20,"price":30.90},
        {"product_name":"tomato3","quantity":20,"price":40.90},
        {"product_name":"tomato4","quantity":20,"price":20.90}
    ]
    return render_template("home.html",product_list=product_list)

@app.route('/product')
def product():
    product_list = Product.query.all()
    return render_template("product/product.html",product_list=product_list)

@app.route('/addproduct',methods=["GET","POST"])
def addproduct():
    if request.method == "POST":
        product = Product(name=request.form["product_name"],category_id=request.form["category_id"])
        db.session.add(product)
        db.session.commit()
        flash(u"{product.name} was successfully added to productlist","success")
        return redirect("/product")
@app.route('/delete/product/<int:id>',methods=["GET","POST"])
def deleteProduct(id):
    product_to_delete = Product.query.get_or_404(id)
    try:
        db.session.delete(product_to_delete)
        db.session.commit()
        return redirect("/product")
    except:
        return "error in deleting"

@app.route('/update/product/<int:id>',methods=["GET","POST"])
def updateproduct(id):
    if request.method == "POST":
        product_to_update = Product.query.get_or_404(id)
        product_to_update.name = request.form["product_name"]
        product_to_update.category_id = request.form["category_id"]
        db.session.commit()
        flash(u"{product.name} was successfully updated to productlist","success")
        return redirect("/product")
    product_to_view = Product.query.get_or_404(id)
    return render_template("product/update_product.html",product=product_to_view)
@app.route('/login',methods=["GET","POST"])
def login():
    if request.method == "POST":
        user = User.query.filter_by(username=request.form["username"])
        if user not None and user.password==request.form["password"]:
            return redirect('/home')
        else:
            return render_template('login.html')
    return render_template('login.html')
    
@app.route('/register',methods=["GET","POST"])
def register():
    if request.method == "POST":
        user = User(firstname=request.form["firstname"],lastname=request.form["lastname"]
        ,username=request.form["username"],password=request.form["password"])
        db.session.add(user)
        db.session.commit()
        return redirect('/login')
    return render_template('register.html')