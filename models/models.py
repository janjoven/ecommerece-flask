from datetime import datetime
from myfirstapp.database import db

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer,primary_key=True)
    firstname = db.Column(db.String(45),nullable=False)
    lastname = db.Column(db.String(45),nullable=False)
    username = db.Column(db.String(20),nullable=False)
    password = db.Column(db.String(20),nullable=False)
    created_at = db.Column(db.DateTime,nullable=False,default=datetime.now)
    updated_at = db.Column(db.DateTime,nullable=False,default=datetime.now,onupdate=datetime.now)

class Product(db.Model):
    __tablename__ = 'products'

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(45),nullable=False)
    category_id = db.Column(db.Integer,db.ForeignKey('category.id'),nullable=False)
    created_at = db.Column(db.DateTime,nullable=False,default=datetime.now)
    updated_at = db.Column(db.DateTime,nullable=False,default=datetime.now,onupdate=datetime.now)
    category = db.relationship("Category",back_populates='product')

class Category(db.Model):
    __tablename__ = 'category'

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(45),nullable=False)
    product = db.relationship("Product",back_populates='category')
    created_at = db.Column(db.DateTime,nullable=False,default=datetime.now)
    updated_at = db.Column(db.DateTime,nullable=False,default=datetime.now,onupdate=datetime.now)

